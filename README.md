# offlineinternet/tinc

A Tinc VPN image for OLIP.

## Image

The entrypoint generates a configuration if none is found:

```text
/etc/tinc   /
└── olipvpn
    ├── hosts
    │   ├── ideascube_8c38 -> /etc/tinc/olipvpn/hosts/public_key
    │   ├── public_key
    │   └── tincmaster
    ├── rsa_key.priv
    ├── tinc.conf
    ├── tinc-down
    ├── generated_mac_first_bits
    └── tinc-up
```

The `public_key` is persisted ; when the MAC changes, the symlink is updated.

The Tinc interface MAC address is built from the host MAC address,
and while the last bits are also derived from the host MAC address,
the generated first bots are persisted as well.

You probably want to bind-mount `/etc/tinc/`
in order to retrieve the generated public key.

## Configuration

The configuration is done via environment variables:

### `VPN_NETWORK_NAME`

The `VPN_NETWORK_NAME` variable is used for the `/etc/tinc/` subdirectory
creation, as well as for the interface name.

### `VPN_HUB_NAME`

The `VPN_HUB_NAME` variable holds the host to `ConnectTo`.

### `VPN_HUB_KEY`

The `VPN_HUB_KEY` variable contains the Hub public key
as a `base64` encoded string, generated as such:

```shell
base64 -w0 /etc/tinc/olipvpn/hosts/tincmaster
```

## Usage

The `NET_ADMIN` capability, the `net=host` and the `privileged`
options allow the `tincd` daemon to use `/dev/net/tun`
and configure the interface.

In the following examples,
we'll use `tincmaster` as aa `VPN_HUB_NAME`,
and `olipvpn` as a `VPN_NETWORK_NAME`.

### Docker run

```shell
docker run -d \
    --name tinc \
    --hostname ct-tinc \
    --net=host \
    --cap-add NET_ADMIN \
    --privileged \
    --env-file .env \
    --volume /data/tinc/tincmaster.pub:/etc/tinc/olipvpn/hosts/tincmaster:ro \
    offlineinternet/tinc
```

### Compose

```yaml
services:
  tinc:
    container_name: tinc
    hostname: ct-tinc
    network_mode: host
    privileged: true
    cap_add:
        - NET_ADMIN
    environment:
        - VPN_NETWORK_NAME=olipvpn
        - VPN_HUB_NAME=tincmaster
    volumes:
        - /data/tinc/config:/etc/tinc/
        - /data/tinc/tincmaster.pub:/etc/tinc/olipvpn/hosts/tincmaster:ro
    image: offlineinternet/tinc
```

## References

- [Tinc documentation](https://tinc-vpn.org/docs/).
