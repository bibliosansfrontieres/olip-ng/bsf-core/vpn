#!/bin/bash

# Required inputs:
# - VPN_NETWORK_NAME: VPN name (i.e. `olipvpn`)
# - VPN_HUB_NAME: central node name (i.e. `tincmaster`)
# - VPN_HUB_KEY: central node public key, base64 encoded (base64 -w0 $keyfile)
# generated:
# - TINC_MAC_ADDRESS: a MAC address for the Tinc interface
# - NODE_NAME: this very device node name (i.e. `ideascube_1234`)

say() {
    echo >&2 "[+] $*"
}
fatal() {
    echo >&2 "[!] FATAL: $*"
    exit 2
}
tinc_generate_first_bits(){
    if [ -r "/etc/tinc/${VPN_NETWORK_NAME}/generated_mac_first_bits" ] ; then
        say "Reuse generated bits..."
        cat "/etc/tinc/${VPN_NETWORK_NAME}/generated_mac_first_bits"
    else
        # generate a MAC address following standards
        say "Generate MAC first bits..."
        hexdump -n 4 -ve '1/1 "%.2x "' /dev/random \
            | awk -v a="2,6,a,e" -v r="$RANDOM" '
                BEGIN {
                    srand(r);
                }
                NR==1 {
                    split(a, b, ",");
                    r=int(rand() * 4 + 1);
                    printf("%s%s:%s:%s:%s", substr($1, 0, 1), b[r], $2, $3, $4);
                }' \
            | tee "/etc/tinc/${VPN_NETWORK_NAME}/generated_mac_first_bits"
    fi
}
build_tinc_mac_address(){
    say "Generate MAC address for Tinc interface..."
    TINC_MAC_ADDRESS="$( tinc_generate_first_bits ):$( cut -d ':' -f 5,6 /olip-files/network/mac-address )"
}
build_tinc_node_name(){
    say "Generate node name for MAC last bits..."
    NODE_NAME="ideascube_$( cat /olip-files/network/mac-id )"
}
tinc_generate_daemon_config(){
    say "Generate daemon configuration file..."
    sed -e "
        s/@@VPN_NETWORK_NAME@@/${VPN_NETWORK_NAME}/;
        s/@@VPN_HUB_NAME@@/${VPN_HUB_NAME}/;
        " /tinc-template/tinc.conf > "/etc/tinc/${VPN_NETWORK_NAME}/tinc.conf"
}
tinc_install_ifupdown_scripts(){
    say "Install ifupdown scripts..."
    cp /tinc-template/tinc-up /tinc-template/tinc-down "/etc/tinc/${VPN_NETWORK_NAME}/"
    build_tinc_mac_address
    sed -i -e "
        s/@@VPN_NETWORK_NAME@@/${VPN_NETWORK_NAME}/;
        s/@@TINC_MAC_ADDRESS@@/${TINC_MAC_ADDRESS}/
        " "/etc/tinc/${VPN_NETWORK_NAME}/tinc-up"
}
tinc_configure_daemon(){
    say "Configure daemon..."
    tinc_generate_daemon_config
    tinc_install_ifupdown_scripts
}
tinc_create_node_keys(){
    say "Create node keys..."
    mkdir -p "/etc/tinc/${VPN_NETWORK_NAME}/hosts/"
    touch "/etc/tinc/${VPN_NETWORK_NAME}/hosts/public_key"
    chmod a+r "/etc/tinc/${VPN_NETWORK_NAME}/hosts/public_key"
    # unattended! https://www.tinc-vpn.org/pipermail/tinc/2014-December/003941.html
    tincd -n "$VPN_NETWORK_NAME" -K4096 </dev/null
}
tinc_symlink_public_key(){
    say "Delete existing symlinks..."
    find "/etc/tinc/${VPN_NETWORK_NAME}/hosts/" -type l -ls -delete
    say "Create symlink..."
    ln -s "/etc/tinc/${VPN_NETWORK_NAME}/hosts/public_key" "/etc/tinc/${VPN_NETWORK_NAME}/hosts/${NODE_NAME}"
}
tinc_reconfigure_node_name(){
    say "Reconfigure node name in tinc.conf (${NODE_NAME})"
    sed -i -e "s/^Name = .*/Name = ${NODE_NAME}/" "/etc/tinc/${VPN_NETWORK_NAME}/tinc.conf"
}
tinc_install_hub_key(){
    say "Install Hub key from \$VPN_HUB_KEY variable..."
    echo "$VPN_HUB_KEY" | base64 -d \
        > "/etc/tinc/${VPN_NETWORK_NAME}/hosts/${VPN_HUB_NAME}"
}
tinc_configure_dhcp_from_host(){
    say "Configure DHCP from host..."
    echo "allow-hotplug olipvpn
iface olipvpn inet dhcp" > /tmp/interfaces.d/vpn.conf
}
tinc_autoconf() {
    say "Autogenerating config for ${VPN_NETWORK_NAME}..."
    mkdir -p "/etc/tinc/${VPN_NETWORK_NAME}/"
    tinc_configure_daemon
    tinc_create_node_keys
}

[ -n "$VPN_NETWORK_NAME" ] \
    || fatal "The VPN_NETWORK_NAME variable is not set."
[ -n "$VPN_HUB_NAME" ] \
    || fatal "The VPN_HUB_NAME variable is not set."
[ -n "$VPN_HUB_KEY" ] \
    || fatal "The VPN_HUB_KEY variable is not set."


say "Provided configuration:
- VPN_NETWORK_NAME: $VPN_NETWORK_NAME
- VPN_HUB_NAME: $VPN_HUB_NAME
- VPN_HUB_KEY: $VPN_HUB_KEY"

say "Check config for ${VPN_NETWORK_NAME}..."
# shellcheck disable=SC2015
if [ -d "/etc/tinc/${VPN_NETWORK_NAME}" ] ; then
    say "Configuration found for ${VPN_NETWORK_NAME}"
else
    say "No Tinc configuration found for ${VPN_NETWORK_NAME}"
    tinc_autoconf
fi

say "Check node keys..."
if [ -r "/etc/tinc/${VPN_NETWORK_NAME}/rsa_key.priv" ] ; then
    say "Private key found for node"
else
    say "No private key found for node"
    tinc_create_node_keys
fi

say "Check Hub (${VPN_HUB_NAME}) public key..."
if [ -r "/etc/tinc/${VPN_NETWORK_NAME}/hosts/${VPN_HUB_NAME}" ] ; then
    say "Hub key found"
else
    say "No Hub key found"
    tinc_install_hub_key
fi

build_tinc_node_name
say "Check public key symlink..."
if [ -L "/etc/tinc/${VPN_NETWORK_NAME}/hosts/${NODE_NAME}" ] ; then
    say "Symlink exists, nothing to do"
else
    say "Symlink not found"
    tinc_symlink_public_key
    tinc_reconfigure_node_name
    tinc_install_ifupdown_scripts
fi


tinc_configure_dhcp_from_host

NODE_KEY="$( base64 -w0 "/etc/tinc/${VPN_NETWORK_NAME}/hosts/public_key" )"

say "Generated values:
- TINC_MAC_ADDRESS: $TINC_MAC_ADDRESS
- NODE_NAME: $NODE_NAME
- NODE_KEY: $NODE_KEY"
export TINC_MAC_ADDRESS NODE_NAME NODE_KEY

say "Generated files:"
for i in tinc.conf tinc-up hosts/public_key ; do
    say "${i}:"
    cat "/etc/tinc/${VPN_NETWORK_NAME}/${i}"
done

say "Wait for internet connection..."
until [ -f /olip-files/network/internet ] ; do sleep 10 ; done

say "Start tincd..."
exec "$@"
