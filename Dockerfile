FROM debian:12-slim

# hadolint ignore=DL3008
RUN export DEBIAN_FRONTEND=noninteractive ; \
    apt-get update --quiet --quiet \
    && apt-get install --no-install-recommends --yes --quiet --quiet \
        bsdextrautils \
        net-tools \
        tinc \
    && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY rootfs/ /

ENTRYPOINT [ "entrypoint.sh" ]
CMD [ "start_tincd.sh" ]
